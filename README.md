## Wordpress Plugin Admin URL

This plugin helps you rename the default wp-admin url to a custom one.
After installing it you have to change wp-config.php and .htaccess files as explained in plugin settings page

![Alt text](screenshot.png?raw=true "Screenshot")

Tested with WP version 4.x

## License

[![License](https://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2015 © <a href="https://gui.com.pt" target="_blank">Gui</a>.